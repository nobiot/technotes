#lang pollen



◊article{
         ◊article-header{}
         ◊title{Adapting Org-Roam for Markdown}
         ◊section{

                  Since I initially wrote this brief note, I have gone a little further and developed a plug-in for ◊extlink["https://org-roam.readthedocs.io/"]{Org Roam}. I named it ◊extlink["https://gitlab.com/nobiot/md-roam"]{Md Roam} (◊code{md-roam}). This has rendered obsolete what follows below; nevertheless, I will keep it for archive◊numbered-note{Also ◊extlink["https://github.com/jethrokuan/org-roam/commit/d7fc91e04793ab50df041cc7a6b9ac5d67c1e63f"]{Org Roam at commit d7fc91e} introduced a configuration option for different extensions. Note that this configuration itself does not support markdown files. Org Roam supports only ◊code{org} files.}.

                  ◊h2{Program Change}

                  Added extraction of titles from markdown files defined in the YAML front matter---well, there are some issues, but it's working OK for my personal use.

                 ◊strike{Only one line of code added}. See my repo above for more detail.

                 I added an extension ◊code{md} in this function---I added an emphasis to illustrate.

                 ◊p{
                                          ◊pre[#:class "fullwidth"]{◊code{
                            org-roam--org-file-p (path)
                            "Check if PATH is pointing to an org file."
                            (let ((ext (org-roam--file-name-extension path)))
                              (or (string= ext "org")
                                  ◊strong{(string= ext "md")})
                              (and
                               (string= ext "gpg")
                               (string= (org-roam--file-name-extension (file-name-sans-extension path)) "org")))
                            }
                         }
                     }


                 ◊h2{Tests}

                 ◊figure["./static/images/01650ED6-2A2F-4C9F-9F5E-4266F6B475A0.GIF"]{Updating the title of a markdown file. Note the back-link side pane is also updated.}

                 ◊figure["./static/images/39FDBCAC-43FC-4187-B05B-2EDD7433C4FD.GIF"]{Inserting a link to another markdown file, and an org file.}

                 ◊figure["./static/images/362AB8B0-DBD0-4745-9C59-87CAD3211619.GIF"]{Testing back links by following a link in the back-link side-window.}

                 }
                 ◊section{◊footer{
                                  Changed on: 2020-04-27
                                  Created on: 2020-03-21}}
                 }
