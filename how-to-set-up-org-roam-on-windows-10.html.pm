#lang pollen

◊article{
         ◊article-header{}
         ◊title{How to set up Org Roam on Windows 10}
         ◊section{
                  ◊extlink["https://org-roam.readthedocs.io/en/master/"]{Org Roam} is an open-source note-taking application available as an ◊extlink["https://www.gnu.org/software/emacs/"]{Emacs} package. It emulates a knowledge management method known as Zettelkasten◊numbered-note{◊extlink["https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125"]{David Clear} provides one of the best descriptions of the Zettelkasten method available in plain English. Additionally, refer to ◊extlink["https://zettelkasten.de/"]{this active blog--forum site} dedicated to the method. Clear draws a lot from it, too} that helps you connect your ideas in a novel way. It is an open-source free alternative for ◊extlink["https://roamresearch.com/"]{Roam}.

                  The additional steps I described below are now truely obsolte. Jethro (the author) and some of us from the community worked to make ◊extlink["https://github.com/org-roam/org-roam/pull/701"]{org-roam to support emacsql-sqlite3}. This means you do not not need to compile or hack the source code; not need for the additional steps. You need to have a sqlite3 binary (.exe file), though. This can be obtained via the official sqlite website, scoop (I use it), and etc.

                  ◊strike{You need to do additional steps to set up ◊code{org-roam} package in Emacs on Windows}◊numbered-note{I can only speak about my experience with Windows 10, but the principle should be applicable to previous versions.}. The steps described here have been incorporated into the ◊extlink["https://org-roam.readthedocs.io/en/master/installation/#windows"]{official documentation}.
                  
                  ◊h2{Steps}

                  ◊ol{
                      ◊li{On Windows, via PowerShell, ◊extlink["https://scoop.sh/"]{scoop} ◊code{emacs}◊numbered-note{To install emacs with scoop, you will need to ◊extlink["https://github.com/lukesampson/scoop/wiki/Buckets#installing-from-other-buckets"]{add extra bucket}.}, ◊strike{◊code{sqlite}}◊numbered-note{First time I set up ◊code{org-roam} for my computer, I was not thorough---i.e. I didn't know what I was talking about. You don't need to scoop ◊code{sqlite}---you include the ◊code{sqlite3.c} file in the make process.}, ◊code{gcc}, and ◊code{make}}


                      ◊li{In Emacs, install ◊code{emacsql} and ◊code{emacsql-sqlite}}


                      ◊li{◊strike{Modify ◊code{Makefile} that comes with ◊code{emacsql-sqlite}}◊numbered-note{Go to the next step---you don't need to alter your Makefile. I realized this only later. I have repeated this procedure a couple of times---it works.}}


                      ◊li{Back in PowerShell, make the ◊code{emacsql-sqlite} executable:

                      ◊pre[#:class "fullwidth"]{◊code{make emacsql-sqlite CC=gcc LDLIBS= }}

                      Use ◊code{gcc} and empty ◊code{LDLIBS} parameter◊numbered-note{I realized that you didn't have to change the Makefile. You run ◊code{make} with passing nothing to the ◊code{LDLIBS} flag.}}

                      
                      ◊li{Back in Emacs, install and configure ◊code{org-roam}}
                      }
                  }
                 ◊section{◊footer{
                                  Changed on: 2020-05-29
                                  Created on: 2020-03-19}}
                 }
